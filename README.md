# Notebooks #

These notebooks run in the notebook environments of both Terrascope and PROBA-V MEP ([https://proba-v-mep.esa.int/notebooks](https://proba-v-mep.esa.int/notebooks)):

These samples are also available when you log in to the notebook environment, under: 'Private/notebook-samples'.

## Updating ##
We make regular updates to these samples. To get the latest changes, run 'git pull' in a terminal window (New->Terminal) after changing to the notebook-samples directory (cd ~/Private/notebook-samples), but be careful in case you made local changes.

## Disclaimer ##
This repository contains notebook samples that can run on Terrascope or PROBA-V MEP. All samples are provided AS-IS, and should not be assumed to be scientifically correct.
